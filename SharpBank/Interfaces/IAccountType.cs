﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Interfaces
{
    public interface IAccountType
    {
        double GetInterest(double amount, Account account);
        string GetStatementStart();
    }
}
