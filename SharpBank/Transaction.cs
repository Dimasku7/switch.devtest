﻿using SharpBank.Interfaces;
using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        public DateTime transactionDate;


        public Transaction(double amount, Account account)
        {
            this.amount = amount;
            this.transactionDate = DateTime.Now;
        }
    }
}
