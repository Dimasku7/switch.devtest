﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public List<Account> GetAcounts()
        {
            return this.accounts;
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            return accounts.Any() ? accounts.Sum(x => x.InterestEarned()) : 0;
        }

        public String GetStatement()
        {
            string statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private String StatementForAccount(Account a)
        {
            //Translate to pretty account type
            string s = a.GetAccountType().GetStatementStart();

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        private String ToDollars(double d)
        {
            return String.Format(CultureInfo.InvariantCulture, "${0:N2}", Math.Abs(d));
        }

        /// <summary>
        /// Transfers an amount from an account to another of the same customer
        /// </summary>
        /// <param name="accountOut">Account that sends the amount</param>
        /// <param name="accountIn">Account that receives the amount</param>
        /// <param name="amount">Amount of the transaction</param>
        public void TransferToLocalAccount(Account accountOut, Account accountIn, double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                accountOut.Withdraw(amount);
                accountIn.Deposit(amount);
            }
        }
    }
}
