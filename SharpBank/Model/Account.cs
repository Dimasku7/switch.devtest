﻿using SharpBank.Interfaces;
using SharpBank.Model.AccountTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {

        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;

        private readonly IAccountType accountType;
        public List<Transaction> transactions;

        public Account(int accountType)
        {
            this.accountType = GetAccountTypeByNumber(accountType);
            this.transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount, this));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount, this));
            }
        }

        public double InterestEarned()
        {
            var sumOfTransactions = SumTransactions();
            return accountType.GetInterest(sumOfTransactions, this);
        }

        public double SumTransactions()
        {
            return transactions
                .Select(x => x.amount)
                .DefaultIfEmpty(0)
                .Sum();
        }

        public IAccountType GetAccountType()
        {
            return accountType;
        }

        public IAccountType GetAccountTypeByNumber(int accountType)
        {
            switch (accountType)
            {
                case SAVINGS:
                    return new AccountTypeSavings();
                case MAXI_SAVINGS:
                    return new AccountTypeMaxiSavings();
                default:
                    return new AccountTypeChecking();
            }
        }

        public int GetDaysOfInterestGenerated()
        {
            var firstDeposit = transactions
                            .OrderBy(x => x.transactionDate)
                            .FirstOrDefault();

            if (firstDeposit == null) return 0;

            return (DateTime.Now - firstDeposit.transactionDate).Days;
        }

        public int GetDaysSinceLastWithdrawal()
        {
            var lastWithdrawal = transactions
                                 .Where(x => x.amount < 0)
                                 .OrderByDescending(x => x.transactionDate)
                                 .FirstOrDefault();

            if (lastWithdrawal == null) return 0;

            return (DateTime.Now - lastWithdrawal.transactionDate).Days;
        }

        /// <summary>
        /// Convert annual interest to daily one
        /// </summary>
        /// <param name="amount">Amount to apply the interest</param>
        /// <param name="rate">Annual rate</param>
        /// <param name="days">Days amount since last withdrawal</param>
        /// <returns>Daily rate</returns>
        public static double GetDailyInterestFromAnual(double amount, double rate, int days)
        {
            return (amount * Math.Pow((1 + rate / 360), days)) - amount;
        }
    }
}
