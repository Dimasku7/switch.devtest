﻿using SharpBank.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Model.AccountTypes
{
    public class AccountTypeMaxiSavings : IAccountType
    {
        public string GetStatementStart()
        {
            return "Maxi Savings Account\n";
        }

        public double GetInterest(double amount, Account account)
        {
            int daysSinceLastWithdrawal = account.GetDaysSinceLastWithdrawal();
            int daysSinceFirstDeposit = account.GetDaysOfInterestGenerated();

            bool existsWithdrawal = account.transactions.Any(x => x.amount < 0);

            if (daysSinceLastWithdrawal > 10 || !existsWithdrawal)
                return Account.GetDailyInterestFromAnual(amount, 0.05, daysSinceFirstDeposit);

            return Account.GetDailyInterestFromAnual(amount, 0.001, daysSinceFirstDeposit);
        }
    }
}
