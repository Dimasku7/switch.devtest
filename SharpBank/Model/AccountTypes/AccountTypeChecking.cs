﻿using SharpBank.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Model.AccountTypes
{
    public class AccountTypeChecking : IAccountType
    {
        public string GetStatementStart()
        {
            return "Checking Account\n";
        }

        public double GetInterest(double amount, Account account)
        {
            int daysSinceFirstDeposit = account.GetDaysOfInterestGenerated();
            return Account.GetDailyInterestFromAnual(amount, 0.001, daysSinceFirstDeposit);
        }
    }
}
