﻿using SharpBank.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Model.AccountTypes
{
    public class AccountTypeSavings : IAccountType
    {
        private double withdrawalLimit = 1000;

        public string GetStatementStart()
        {
            return "Savings Account\n";
        }

        public double GetInterest(double amount, Account account)
        {
            int daysSinceFirstDeposit = account.GetDaysOfInterestGenerated();

            if (amount <= withdrawalLimit)
                return Account.GetDailyInterestFromAnual(amount, 0.001, daysSinceFirstDeposit);
            else
            {
                double firstLimitInterest = Account.GetDailyInterestFromAnual(withdrawalLimit, 0.001, daysSinceFirstDeposit);
                double remainderInterest = Account.GetDailyInterestFromAnual(amount - withdrawalLimit, 0.002, daysSinceFirstDeposit);
                return firstLimitInterest + remainderInterest;
            }
        }
    }
}
