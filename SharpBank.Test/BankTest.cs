﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Account(Account.CHECKING));
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.CHECKING);
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);
            var firstTransaction = checkingAccount.transactions[0];
            firstTransaction.transactionDate = DateTime.Now.AddDays(-300);


            Assert.AreEqual(0.08336794936616343, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account savingAccount = new Account(Account.SAVINGS);
            bank.AddCustomer(new Customer("Bill").OpenAccount(savingAccount));

            savingAccount.Deposit(1500.0);
            var firstTransaction = savingAccount.transactions[0];
            firstTransaction.transactionDate = DateTime.Now.AddDays(-300);

            Assert.AreEqual(1.6677053387406886, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithoutWithdrawals()
        {
            Bank bank = new Bank();
            Account maxiSavingsAccount = new Account(Account.MAXI_SAVINGS);
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiSavingsAccount));

            maxiSavingsAccount.Deposit(3000.0);
            var firstTransaction = maxiSavingsAccount.transactions[0];
            firstTransaction.transactionDate = DateTime.Now.AddDays(-300);

            Assert.AreEqual(127.63166653462031d, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithoutWithdrawalsInTenDays()
        {
            Bank bank = new Bank();
            Account maxiSavingsAccount = new Account(Account.MAXI_SAVINGS);
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiSavingsAccount));

            maxiSavingsAccount.Deposit(3000.0);
            maxiSavingsAccount.Withdraw(1000.0);

            maxiSavingsAccount.transactions.ForEach(x => x.transactionDate = new DateTime(2019, 01, 01));

            Assert.AreEqual(14.77551154581397d, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithWithdrawalsInTenDays()
        {
            Bank bank = new Bank();
            Account maxiSavingsAccount = new Account(Account.MAXI_SAVINGS);
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiSavingsAccount));

            maxiSavingsAccount.Deposit(3000.0);
            var firstTransaction = maxiSavingsAccount.transactions[0];
            firstTransaction.transactionDate = DateTime.Now.AddDays(-300);
            maxiSavingsAccount.Withdraw(1000.0);

            Assert.AreEqual(1.6673589873232686d, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }
    }
}
